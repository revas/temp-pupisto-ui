import Vue from 'vue'
import Vuex from 'vuex'
import {sync} from 'vuex-router-sync'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
import RevasRouter from './router'
import RevasComponents from '@revas/design/vue'

import RevasAuth from './store/auth'
import RevasApp from './store'

import App from './App'

import VueSpaceship from './plugins/spaceship'
import VueIntercom from 'vue-intercom'

import { loadLanguageAsync, i18n } from './i18n'

export default {
  configure: (options) => {
    const $http = Axios.create({
      baseURL: options.base.api.url
    })
    return {
      options: options,
      http: $http
    }
  },
  install: (name, config, modules, routes) => {
    const options = config.options
    const $http = config.http
    if (options.platforms && options.platforms.sentry) {
      Raven
        .config(options.platforms.sentry.dsn, {
          environment: options.mode
        })
        .addPlugin(RavenVue, Vue)
        .install()
    }

    Vue.use(Vuex)
    Vue.use(VueAxios, $http)
    Vue.use(VueIntercom, {
      appId: options.platforms.intercom.id
    })
    Vue.use(RevasComponents)
    Vue.use(VueSpaceship, {
      current: name,
      domain: options.base.domain
    })

    modules.auth = RevasAuth.buildStore(options.base.domain, options.auth.oauth2)
    modules.app = RevasApp.buildStore($http)

    const store = new Vuex.Store({
      strict: true,
      modules: modules
    })

    const router = RevasRouter.buildRouter(routes, options.router)

    sync(store, router)

    loadLanguageAsync('it-IT')

    return store.dispatch('auth/handle').then(() => {
      return {
        vue: new Vue({
          el: '#app',
          router,
          store,
          i18n,
          components: {App},
          template: '<App/>'
        }),
        http: $http
      }
    })
  }
}

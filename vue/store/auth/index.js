import types from './types'
import Authenticator from '../../../core/auth/auth0/index'

const buildStore = (domain, oauth2Config) => {
  let authConfig = Object.assign({}, Authenticator.config(), oauth2Config)
  const authenticator = new Authenticator(authConfig)

  return {
    namespaced: true,
    state: {
      accessToken: authenticator.load().accessToken,
      expiresAt: authenticator.load().expiresAt,
      errors: []
    },
    getters: {
      accessToken: (state) => {
        return state.accessToken
      },
      expiresAt: (state) => {
        return state.expiresAt
      },
      isAuthenticated: (state) => {
        return !!state.accessToken
      },
      hasErrors: (state) => {
        return state.errors.length > 0
      },
      errors: (state) => {
        return state.errors
      }
    },
    mutations: {
      [types.LOGIN_SUCCESS] (state, payload) {
        authenticator.storeAccessToken(domain, payload.accessToken, payload.expiresAt)
        state.accessToken = payload.accessToken
        state.expiresAt = payload.expiresAt
      },
      [types.LOGIN_ERROR] (state, exception) {
        authenticator.clearAccessToken()
        state.errors.push(exception)
      },
      [types.LOGOUT_SUCCESS] () {
        authenticator.clearAccessToken(domain)
      }
    },
    actions: {
      async handle ({state, commit}) {
        try {
          const data = await authenticator.handle()
          commit(types.LOGIN_SUCCESS, data)
        } catch (ex) {
          let errorCode = ''
          if (ex) {
            errorCode = Authenticator.checkErrorKey(ex.errorDescription)
          }
          if (errorCode === 'ERROR_EMAIL_NOT_VERIFIED') {
            commit(types.LOGIN_ERROR, 'AUTH_LOGIN_ERROR_EMAIL_NOT_VERIFIED')
          } else {
            if (!state.accessToken) {
              authenticator.login()
            }
          }
        }
      },
      async refresh ({state, commit}) {
        try {
          const data = await authenticator.refresh()
          commit(types.LOGIN_SUCCESS, data)
        } catch (ex) {
          authenticator.login()
        }
      },
      logout ({commit}) {
        commit(types.LOGOUT_SUCCESS)
        authenticator.logout()
      }
    }
  }
}

export default { buildStore }

const VueSpaceship = {
  install (Vue, options) {
    Vue.prototype.$spaceship = {
      navigate (revoName) {
        const routes = {
          'animo': 'https://account.' + options.domain,
          'universo': 'https://universe.' + options.domain,
          'alianco': 'https://organizations.' + options.domain,
          'membro': 'https://members.' + options.domain,
          'alianco-create': 'https://organizations.' + options.domain + '/create',
          'stenografo': 'https://documents.' + options.domain + '/editor'
        }
        let foundRoute = null
        if (options.current !== revoName) {
          for (let revo in routes) {
            if (revo === revoName) {
              foundRoute = routes[revo]
            }
          }
        }
        if (foundRoute) {
          window.location.href = foundRoute
        }
      }
    }
  }
}

export default VueSpaceship

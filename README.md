# Revas UI

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat-square)](http://standardjs.com)

Libraries, tools and helpers to build Revas Vue applications.

## Authentication

You can use the ready authentication Vuex store to build your app.

```
const authStoreConfig = {
    domain: process.env.VUE_APP_OAUTH_DOMAIN,
    clientID: process.env.VUE_APP_OAUTH_CLIENT_ID,
    redirectUri: process.env.VUE_APP_OAUTH_REDIRECT_URI,
    audience: process.env.VUE_APP_OAUTH_AUDIENCE
}
```

```
import { makeAuthStore } from '@revas/ui/vue'

const authStore = makeAuthStore(authStoreConfig)
```

```
import Revas from '../../../vue'

Revas.install({}, [], {
  router: {
    default: '/'
  }
})
```